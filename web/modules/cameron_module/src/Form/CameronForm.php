<?php

/**

 * @file

 * Contains \Drupal\cameron_module\Form.

 */

namespace Drupal\cameron_module\Form;

use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Form\FormStateInterface;

class CameronForm extends ConfigFormBase {

  /**

   * {@inheritdoc}

   */

  public function getFormId() {

    return 'cameron_module_config_form';

  }

  /**

   * {@inheritdoc}

   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('cameron_module.settings');

    $form['featured_developments_icon_1'] = array(

      '#type' => 'textfield',

      '#title' => $this->t('Featured Developments Icon 1 URL:'),

      '#default_value' => $config->get('featured_developments_icon_1'),
      '#description' => 'Featured Developments Icon Location',

      '#required' => TRUE,

    );

    $form['featured_developments_icon_2'] = array(

      '#type' => 'textfield',

      '#title' => $this->t('Featured Developments Icon 2 URL:'),

      '#default_value' => $config->get('featured_developments_icon_2'),

      '#description' => 'Featured Developments Icon Bedrooms',

      '#required' => TRUE,

    );

    $form['featured_developments_icon_3'] = array(

      '#type' => 'textfield',

      '#title' => $this->t('Featured Developments Icon 3 URL:'),

      '#default_value' => $config->get('featured_developments_icon_2'),

      '#description' => 'Featured Developments Icon Price',

      '#required' => TRUE,

    );
    /*$form['unicef_live_chat_url_mode'] = array(
      '#type' => 'select',
      '#title' => $this->t('Live Chat Mode:'),
      '#options' => array(
        '1' => 'Enable',
        '0' => 'Disable'
      ),
      '#default_value' => $config->get('unicef_live_chat_url_mode'),
      '#description' => $this->t('Enable module live chat'),
    );*/

    return $form;

  }

  /**

   * {@inheritdoc}

   */

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cameron_module.settings');
    //$config->set('unicef_paydollar_payment_url', $form_state->getValue('unicef_paydollar_payment_url'));
    $config->set('featured_developments_icon_1', $form_state->getValue('featured_developments_icon_1'));
    $config->set('featured_developments_icon_2', $form_state->getValue('featured_developments_icon_2'));
    $config->set('featured_developments_icon_3', $form_state->getValue('featured_developments_icon_3'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**

   * {@inheritdoc}

   */

  protected function getEditableConfigNames() {

    return [

      'cameron_module.settings',

    ];

  }

}
