<?php
namespace Drupal\cameron_module;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;



class TwigMediaExtension extends AbstractExtension  {
  /**
   * This is the same name we used on the services.yml file
   */
  public function getName() {
    return 'cameron_module.twig_media_extension';
  }



  // Basic definition of the filter. You can have multiple filters of course.
  public function getFilters() {
    return [
      new TwigFilter('media_full_url', [$this, 'getMediaFullUrl']),
      new TwigFilter('cast_to_array', array($this, 'castToArray')),
      new TwigFilter('image_file_url', array($this, 'getImageFileUrl')),
      new TwigFilter('get_node', array($this, 'getNode')),
      new TwigFilter('get_arr_node', array($this, 'getArrNode')),
      new TwigFilter('get_our_developments_form', array($this, 'getOurDevelopmentsForm'))
    ];
  }

  public function getOurDevelopmentsForm($key){
    return isset($_REQUEST[$key])?$_REQUEST[$key]:'';
  }

  public function getArrNode($nodeIds) {
    $nodeIds = explode(",",$nodeIds);
    $response = array();
    foreach ($nodeIds as $nodeId) {
      if(isset($nodeId) ) {
          $node = \Drupal\node\Entity\Node::load(intval($nodeId));
          if($node){
            $response[] = $node->toArray();
          }
      }
    }
    return $response;
  }

  public function getNode($nodeId) {
    $result='';

    if(isset($nodeId)) {
        $node = \Drupal\node\Entity\Node::load(intval($nodeId));
        $result = $node->toArray();
    }
    return $result;
  }

  public function castToArray($stdClassObject) {
      $response = array();
      foreach ($stdClassObject as $key => $value) {
          $response[] = array($key, $value);
      }
      return $response;
  }
  // The actual implementation of the filter.
  public function getMediaFullUrl($mediaId) {
    if(isset($mediaId)) {
        $media = Media::load($mediaId);
        $fid = $media->getSource()->getSourceFieldValue($media);
        $file = File::load($fid);
        //$file = File::load($mediaId);
        $uri = $file->url();
        /*$stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
        $file_path = $stream_wrapper_manager->realpath();*/
        return $uri;
    }
    return "url-not-found";
  }
  //get full Image URL from target_id
  public function getImageFileUrl($targetId){
    if(isset($targetId)) {
      // Load the image file.
      $file = File::load($targetId);
      // Get URI of the image file.
      $file_uri = $file->getFileUri();
      // With the URI, generate a url for non-image style background image.
      $image_path = file_url_transform_relative(file_create_url($file_uri));
      return $image_path;
    }
    return "url-not-found-".$targetId;
  }
}
