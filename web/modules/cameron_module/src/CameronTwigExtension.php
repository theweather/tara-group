<?php

namespace Drupal\cameron_module;

use Drupal\Component\Utility\SortArray;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Block\TitleBlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Masterminds\HTML5\Exception;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\Validator\Constraints\Count;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;

/**
 * Twig extension with some useful functions and filters.
 *
 * Dependency injection is not used for performance reason.
 */
class CameronTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('render_menu', [$this, 'renderMenu']),
      new TwigFunction('get_development', [$this, 'getDevelopment']),
      new TwigFunction('get_slide_show', [$this, 'getSlideShow']),
      new TwigFunction('get_hometype_by_plots', [$this, 'getHomeTypeByPlots']),
      new TwigFunction('get_development_by_plots', [$this, 'getDevelopmentByPlots']),
      new TwigFunction('get_first_home_plan_by_plots', [$this, 'getFirstHomePlanByPlots']),
      new TwigFunction('get_development_by_show_home', [$this, 'getDevelopmentByShowHomePlots']),
      new TwigFunction('get_development_by_view_plot_list', [$this, 'getDevelopmentbyViewPlotList']),
      new TwigFunction('get_house_type', array($this, 'getHouseType'))
    ];
  }

  public function getHouseType(){
    $result=[];
    $nids = \Drupal::entityQuery('node')->accessCheck(FALSE)->condition('type','house_type')->execute();
    $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);

    foreach ($nodes as $key => $node) {
      $result[]=$node->toArray();
    }
    return $result;
  }

  public function getDevelopmentByShowHomePlots(){
    $result='';
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $nodePlots = \Drupal::routeMatch()->getParameter('node');
    $showHomeDevelopmentId='';
    if(isset($nodePlots->type->entity) && $nodePlots->type->entity->label()=='Plots'){
      //if(!$nodePlots->get('field_plots_show_home')->value){
        $plotId=$nodePlots->id();
        $houseTypeId = $nodePlots->field_plots_house_type->target_id;
        $developmentId = $nodePlots->field_plots_development->target_id;
        $objDevelopment = \Drupal\node\Entity\Node::load(intval($developmentId));
        if(!empty($houseTypeId) && !empty($developmentId)){
          $get_data = $this->callAPI('GET', $host.'/api/plot_development/'.$plotId.'/'.$houseTypeId.'/'.$developmentId, false);
          $responses = json_decode($get_data, true);
          if(count($responses)>1)
          {
            $showHomeDevelopmentId = $this->caculateDistance($responses,$objDevelopment->get('field_development_latitude')->value,$objDevelopment->get('field_development_longitude')->value);
          }elseif (count($responses)==1) {
            $showHomeDevelopmentId = $responses[0]['field_plots_development'];
          }
        }
        if(!empty($showHomeDevelopmentId)){
          $node = \Drupal\node\Entity\Node::load(intval($showHomeDevelopmentId));
          $result = $node->toArray();
        }
      //}
    }
    return $result;
  }

  function caculateDistance($responses,$lat1, $lon1){
    $min=PHP_INT_MAX;
    $developmentId='';
    foreach ($responses as $key => $value) {
        $distance=$this->distance($lat1, $lon1, $value['field_development_latitude'], $value['field_development_longitude']);
        if(floatval($distance) < $min){
          $developmentId = $value['field_plots_development'];
          $min=$distance;
        }
    }
    return $developmentId;
  }

  function distance($lat1, $lon1, $lat2, $lon2, $unit="K") {
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
      return 0;
    }
    else {
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
        return ($miles * 0.8684);
      } else {
        return $miles;
      }
    }
  }

  function callAPI($method, $url, $data){
     $curl = curl_init();
     switch ($method){
        case "POST":
           curl_setopt($curl, CURLOPT_POST, 1);
           if ($data)
              curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
           break;
        case "PUT":
           curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
           if ($data)
              curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
           break;
        default:
           if ($data)
              $url = sprintf("%s?%s", $url, http_build_query($data));
     }
     // OPTIONS:
     curl_setopt($curl, CURLOPT_URL, $url);
     curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
     ));
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
     // EXECUTE:
     $result = curl_exec($curl);
     if(!$result){die("Connection Failure");}
     curl_close($curl);
     return $result;
  }

  public function getFirstHomePlanByPlots(){
    $result='';
    $nodePlots = \Drupal::routeMatch()->getParameter('node');
    if(isset($nodePlots->type->entity) && $nodePlots->type->entity->label()=='Plots'){
      $homeTypeId = $nodePlots->field_plots_paragraph->target_id;
      if(!empty($homeTypeId)){
        $objectPlots = \Drupal\node\Entity\Node::load($homeTypeId);
        $result = $objectPlots->toArray();
      }
    }
    return $result;
  }

  public function getHomeTypeByPlots(){
    $result='';
    $nodePlots = \Drupal::routeMatch()->getParameter('node');
    if(isset($nodePlots->type->entity) && $nodePlots->type->entity->label()=='Plots'){
      $homeTypeId = $nodePlots->field_plots_house_type->target_id;
      if(!empty($homeTypeId)){
        $objectPlots = \Drupal\node\Entity\Node::load($homeTypeId);
        $result = $objectPlots->toArray();
      }
    }
    return $result;
  }

  public function getSlideShow($slideShowId) {
    $result='';
    if(isset($slideShowId)) {
        $node = \Drupal\node\Entity\Node::load($slideShowId);
        $result = $node->toArray();
    }
    return $result;
  }

  public function getDevelopmentByPlots(){
    $result='';
    $nodePlots = \Drupal::routeMatch()->getParameter('node');
    if(isset($nodePlots->type->entity) && $nodePlots->type->entity->label()=='Plots'){
      $developmentId = $nodePlots->field_plots_development->target_id;
      if(!empty($developmentId)){
        $objectDevelopment = \Drupal\node\Entity\Node::load($developmentId);
        $result = $objectDevelopment->toArray();
      }
    }
    return $result;
  }

  public function getDevelopment(){
    $result='';
    $node = \Drupal::routeMatch()->getParameter('node');
    if(isset($node->type->entity) && $node->type->entity->label()=='Development'){
        $result = $node->toArray();
    }
    return $result;
  }

  public function getDevelopmentbyViewPlotList(){
    \Drupal::request()->request->get('name');
  }

  public function renderMenu() {
      $menu_tree = \Drupal::menuTree();
      $menu_name = "main";
      // Build the typical default set of menu tree parameters.
      $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);

      // Load the tree based on this set of parameters.
      $tree = $menu_tree->load($menu_name, $parameters);

      // Transform the tree using the manipulators you want.
      $manipulators = array(
          // Only show links that are accessible for the current user.
          array('callable' => 'menu.default_tree_manipulators:checkAccess'),
          // Use the default sorting of menu links.
          array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
      );
      $tree = $menu_tree->transform($tree, $manipulators);

      // Finally, build a renderable array from the transformed tree.
      $menu = $menu_tree->build($tree);

      return array('#markup' => \Drupal::service('renderer')->render($menu));
  }


  /**
   * Checks whether the render array is a field's render array.
   *
   * @param $build
   *   The renderable array.
   *
   * @return bool
   *   True if $build is a field render array.
   */
  protected function isFieldRenderArray($build) {

    return isset($build['#theme']) && $build['#theme'] == 'field';
  }

}

