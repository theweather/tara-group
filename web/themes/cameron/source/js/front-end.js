document.documentElement.className = document.documentElement.className.replace('no-js', 'js');

var $ = jQuery.noConflict(),
  mast = $('.masthead'),
  mastHeight = mast.outerHeight(),
  velocity = 400,
  easing = "cubic-bezier(.19, 1, .22, 1)";

$(function () {

  menu();
  tabs();
  map();
  accordion();
  reveal();
  searchFilter();
  cookieMessage();
  smoothScroll();
  scrollAnchor();
  hero();
  stickyActions();
  modalOverlay();
  carousel();
  gallery();
  general();

});

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function general() {

  if (document.querySelector('.block-panel') !== null) {

    $('.panel-simple:last, .panel-advanced:last').addClass('block-spacing');

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function modalOverlay() {

  if (document.querySelector('.modal-overlay') !== null) {

    var overlay_active = "overlay-active";

    $('.overlay-trigger').each(function () {

      $(this).on('click', function (e) {

        var overlay_id = $(this).attr('href');

        $(overlay_id).addClass(overlay_active);

        $('#master').addClass('overlay-on');

        e.preventDefault();

      });

    });

    $(document).on('click', '.overlay-close', function (e) {

      $('.modal-overlay').removeClass(overlay_active);

      $('#master').removeClass('overlay-on');

      e.preventDefault();

    });

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function stickyActions() {

  if (document.querySelector('.nav-bar') !== null) {

    var stickyNavTop = $('.nav-bar').offset().top;

    var stickyNav = function () {

      var scrollTop = $(window).scrollTop();

      if (scrollTop > stickyNavTop - mastHeight) {

        $('.nav-bar').addClass('sticky');

      } else {

        $('.nav-bar').removeClass('sticky');

      }

    };

    stickyNav();

    $(window).on('scroll resize', function () {

      stickyNav();

    });

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function hero() {

  if (document.querySelector('#video-feature') !== null) {

    var triggerArea = $('.hero-huge + section'),
        video = document.getElementById('video-feature');

    videoAction();

    $(window).on('scroll resize', function () {

      videoAction();

    });

    function videoAction() {

      if ($(this).scrollTop() > triggerArea.offset().top - mastHeight * 2) {

        video.pause();

      } else {

        video.play();

      }

    }

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function accordion() {

  $('.reveal-item').each(function () {

    $(this).find('.reveal-trigger').on('click', function (e) {

      $(this).closest('.reveal-item').toggleClass('open').removeClass('off').siblings().toggleClass('off').removeClass('open');

      if ($(this).closest('.reveal-item').is('.open')) {

        var ele = $(this).closest('.reveal-item').find('.reveal-info');

        $('html, body').animate({scrollTop: $(ele).offset().top - mastHeight * 2}, velocity);

      }

      e.preventDefault();

    });

  });

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function cookieMessage() {

  var cookieAccepted = localStorage.getItem('cookieAccepted');

  if (cookieAccepted) {

    cookieAcceptedActions();

  }

  $(document).on('click', '.cookie-accept', function (e) {

    cookieAcceptedActions();

    localStorage.setItem('cookieAccepted', true);

    e.preventDefault();

  });

  function cookieAcceptedActions() {

    $('#master').addClass('cookie-accepted');

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function searchFilter() {

  if (document.querySelector('.block-search') !== null) {

    $(document).on('click', '.options-toggle', function (e) {

      $(this).toggleClass('on');

      $(this).closest('.block-search').toggleClass('open');

      $('html, body').animate({scrollTop: $('.block-search').offset().top - mastHeight}, velocity);

      e.preventDefault();

    });

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function reveal() {

  if (document.querySelector('.reveal') !== null) {

    $(document).on('click', '.reveal-toggle', function (e) {

      var buttonText = $(this).text();

      if (buttonText == "Read more") {

        $(this).text("Read less");

      } else {

        $(this).text("Read more");

      }

      $(this).prev().toggleClass('open');

      e.preventDefault();

    });

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function menu() {

  $(document).on('click', '.menu-toggle', function (e) {

    $('#master').toggleClass('menu-open');

    e.preventDefault();

  });

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function tabs() {

  $('.tabs').each(function () {

    var tab_active = "active";

    $('.tab-title a').on('click', function (e) {

      var tab_id = $(this).attr('href');

      $(this).parent().addClass(tab_active).siblings().removeClass(tab_active);

      $(tab_id).addClass(tab_active).siblings().removeClass(tab_active);

      $('html, body').animate({scrollTop: $(tab_id).offset().top - mastHeight}, velocity);

      e.preventDefault();

    });

  });

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function carousel() {

  if (document.querySelector('.block-carousel') !== null) {

    var slidersSingle = document.querySelectorAll('.glide-single');

    for (var i = 0; i < slidersSingle.length; i++) {

      var glideSingle = new Glide(slidersSingle[i], {

        type: 'slider',
        rewind: false,
        perView: 1,
        gap: 0

      })

      glideSingle.mount();

    }

  }

  if (document.querySelector('.block-gallery') !== null) {

    var slidersGroup = document.querySelectorAll('.glide-group');

    for (var a = 0; a < slidersGroup.length; a++) {

      var glideGroup = new Glide(slidersGroup[a], {

        type: 'slider',
        bound: true,
        rewind: false,
        perView: 5,
        gap: 0,
        breakpoints: {
          991: {
            perView: 3
          },
          767: {
            perView: 2
          },
          575: {
            perView: 1
          }
        }

      })

      glideGroup.mount();

    }

  }

  if (document.querySelector('.block-preview') !== null) {

    var slidersMultiple = document.querySelectorAll('.glide-multiple');

    for (var k = 0; k < slidersMultiple.length; k++) {

      var glideMultiple = new Glide(slidersMultiple[k], {

        type: 'carousel',
        perView: 2,
        gap: 30,
        breakpoints: {
          991: {
            perView: 1
          }
        }

      })

      glideMultiple.mount();

    }

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function gallery() {

  var initPhotoSwipeFromDOM = function (gallerySelector) {

    var parseThumbnailElements = function (el) {
      var thumbElements = $(el).find('.photoswipe-item:not(.glide__slide--clone)').get(),
        numNodes = thumbElements.length,
        items = [],
        figureEl,
        linkEl,
        size,
        item;

      for (var i = 0; i < numNodes; i++) {

        figureEl = thumbElements[i];

        if (figureEl.nodeType !== 1) {
          continue;
        }

        linkEl = figureEl.children[0];

        size = linkEl.getAttribute('data-size').split('x');

        if ($(linkEl).data('type') == 'lightbox') {
          item = {
            html: $(linkEl).find('.lightbox').html()
          };
        } else {
          item = {
            src: linkEl.getAttribute('href'),
            w: parseInt(size[0], 10),
            h: parseInt(size[1], 10)
          };
        }

        if (figureEl.children.length > 1) {
          item.title = $(figureEl).find('.caption').html();
        }

        if (linkEl.children.length > 0) {
          item.msrc = linkEl.children[0].getAttribute('src');
        }

        item.el = figureEl;
        items.push(item);
      }

      return items;
    };

    var closest = function closest(el, fn) {
      return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    function hasClass(element, cls) {
      return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }

    var onThumbnailsClick = function (e) {
      e = e || window.event;
      e.preventDefault ? e.preventDefault() : e.returnValue = false;

      var eTarget = e.target || e.srcElement;

      var clickedListItem = closest(eTarget, function (el) {
        return (hasClass(el, 'photoswipe-item'));
      });

      if (!clickedListItem) {
        return;
      }

      var clickedGallery = clickedListItem.closest('.photoswipe-wrapper'),
        childNodes = $(clickedListItem.closest('.photoswipe-wrapper')).find('.photoswipe-item:not(.glide__slide--clone)').get(),
        numChildNodes = childNodes.length,
        nodeIndex = 0,
        index;

      for (var i = 0; i < numChildNodes; i++) {
        if (childNodes[i].nodeType !== 1) {
          continue;
        }

        if (childNodes[i] === clickedListItem) {
          index = nodeIndex;
          break;
        }
        nodeIndex++;
      }

      if (index >= 0) {
        openPhotoSwipe(index, clickedGallery);
      }
      return false;
    };

    var photoswipeParseHash = function () {
      var hash = window.location.hash.substring(1),
        params = {};

      if (hash.length < 5) {
        return params;
      }

      var vars = hash.split('&');
      for (var i = 0; i < vars.length; i++) {
        if (!vars[i]) {
          continue;
        }
        var pair = vars[i].split('=');
        if (pair.length < 2) {
          continue;
        }
        params[pair[0]] = pair[1];
      }

      if (params.gid) {
        params.gid = parseInt(params.gid, 10);
      }

      return params;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
      var pswpElement = document.querySelectorAll('.pswp')[0],
        gallery,
        options,
        items;

      items = parseThumbnailElements(galleryElement);

      options = {

        closeOnScroll: false,

        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

        getThumbBoundsFn: function (index) {
          var thumbnail = items[index].el.getElementsByTagName('img')[0],
            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            rect = thumbnail.getBoundingClientRect();

          return {
            x: rect.left,
            y: rect.top + pageYScroll,
            w: rect.width
          };
        }

      };

      if (fromURL) {
        if (options.galleryPIDs) {
          for (var j = 0; j < items.length; j++) {
            if (items[j].pid == index) {
              options.index = j;
              break;
            }
          }
        } else {
          options.index = parseInt(index, 10) - 1;
        }
      } else {
        options.index = parseInt(index, 10);
      }

      if (isNaN(options.index)) {
        return;
      }

      if (disableAnimation) {
        options.showAnimationDuration = 0;
      }

      gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
      gallery.init();

      gallery.listen('beforeChange', function () {
        var currItem = $(gallery.currItem.container);
        $('.pswp__video').removeClass('active');
        var currItemIframe = currItem.find('.pswp__video').addClass('active');
        $('.pswp__video').each(function () {
          if (!$(this).hasClass('active')) {
            $(this).attr('src', $(this).attr('src'));
          }
        });
      });

      gallery.listen('close', function () {
        $('.pswp__video').each(function () {
          $(this).attr('src', $(this).attr('src'));
        });
      });

    };

    var galleryElements = document.querySelectorAll(gallerySelector);

    for (var i = 0, l = galleryElements.length; i < l; i++) {
      galleryElements[i].setAttribute('data-pswp-uid', i + 1);
      galleryElements[i].onclick = onThumbnailsClick;
    }

    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
      openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }

  };

  initPhotoSwipeFromDOM('.photoswipe-wrapper');

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function map() {

  if (document.querySelector('.block-map') !== null) {

    var mapStyle = [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#c9c9c9"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }
    ];

    var iconImagePath = '/themes/cameron/build/images/map/';

    var activeInfoWindow;

    function initMap() {

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8.5,
        center: {lat: 52.2107832, lng: -1.7603051},
        styles: mapStyle,
        disableDefaultUI: true
      });

      var markers = locationsJson.map(function (location, i) {

        var marker_title = location.title,
          marker_category = location.category,
          marker_image = location.image,
          marker_phone = location.phone,
          marker_link_booking = location.link_booking,
          marker_address = location.address,
          marker_bedrooms = location.bedrooms,
          marker_price = location.price,
          marker_summary = location.summary,
          marker_link_siteplan = location.link_siteplan,
          marker_link_development = location.link_development,
          marker_link_plots = location.link_plots;

        var marker_detail =
          '<aside class="listing-preview listing-map">' +

          '<span class="sr-only">' + marker_category + '</span>' +

          '<div class="marker-info-close"><span class="sr-only">x</span></div>' +

          '<a class="image-link" href="' + marker_link_development + '"><figure class="media thin"><picture class="image"><img src="' + marker_image + '" alt="' + marker_title + '" loading="lazy"></picture></figure></a>' +

          '<header class="heading"><h3><a class="heading-link" href="' + marker_link_development + '">' + marker_title + '</a></h3></header>' +

          '<ul class="contact">' +

          '<li class="item phone icon-text icon-phone smaller"><a href="tel:' + marker_phone + '">' + marker_phone + '</a></li>' +
            (marker_link_booking?'<li class="item booking"><a class="button tertiary" href="' + marker_link_development + '">Book your private viewing</a></li>':'') +


          '</ul>' +

          '<ul class="features">' +

          '<li class="item icon-text icon-pin">' + marker_address + '</li>' +
          '<li class="item icon-text icon-beds">' + marker_bedrooms + '</li>' +
          (marker_price!=0?'<li class="item icon-text icon-price">' + marker_price + '</li>':'') +

          '</ul>' +

          '<div class="summary editor"><p>' + marker_summary + '</p></div>' +

          '<ul class="inline-group actions">' +

          '<li class="item"><a class="button" href="' + marker_link_development + '">View development</a></li>' +
          (marker_link_plots?'<li class="item"><a class="button secondary" href="' + marker_link_plots + '">View homes</a></li>':'') +
          (marker_link_siteplan?'<li class="item"><a class="button tertiary" href="' + marker_link_siteplan + '">View  the siteplan</a></li>':'') +


          '</ul>' +

          '</aside>';

        var iconWidth = 40,
          iconHeight = 47,
          iconImage = null;

        if (marker_category.length) {

          iconImage = iconImagePath + marker_category + '.svg';

        } else {

          iconImage = iconImagePath + 'marker.svg';

        }

        var icon = {
          url: iconImage,
          scaledSize: new google.maps.Size(iconWidth, iconHeight)
        };

        var marker = new google.maps.Marker({

          position: location,
          icon: icon,
          animation: google.maps.Animation.DROP

        });

        var infowindow = new google.maps.InfoWindow({

          content: marker_detail

        });

        google.maps.event.addListener(marker, 'click', function (event) {

          activeInfoWindow && activeInfoWindow.close();

          infowindow.open(map, marker);

          map.panTo(this.getPosition());

          activeInfoWindow = infowindow;

        });

        google.maps.event.addListener(infowindow, 'domready', function () {

          var closeBtn = $('.marker-info-close').get();

          google.maps.event.addDomListener(closeBtn[0], 'click', function () {

            infowindow.close();

          });

        });

        google.maps.event.addListener(map, 'click', function (event) {

          if (infowindow) {

            infowindow.close();

          }

        });

        return marker;

      });

      var markerClusterImage = iconImagePath + 'cluster.svg';

      var markerClusterOptions = {

        styles: [{
          width: 40,
          height: 47,
          className: 'marker-cluster',
          url: markerClusterImage
        }]

      };

      var markerCluster = new MarkerClusterer(map, markers, markerClusterOptions);

    }

    google.maps.event.addDomListener(window, "load", initMap);

  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function smoothScroll() {

  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('.tab-title a')
    .not('.overlay-trigger')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').stop().animate({
            scrollTop: target.offset().top - mastHeight
          }, velocity, function () {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) { // Checking if the target was focused
              return false;
            } else {
              $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
            }
            ;
          });
        }
      }
    });

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function scrollAnchor() {

  var hash= window.location.hash
  if ( hash == '' || hash == '#' || hash == undefined ) return false;
  var target = $(hash);
  target = target.length ? target : $('[name=' + hash.slice(1) +']');
  if (target.length) {
    $('html,body').stop().animate({
      scrollTop: target.offset().top - mastHeight
    }, velocity);
  }

}

/*/////////////////////////////////////////////////////////////////////////////////////////*/
