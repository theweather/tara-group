
jQuery(function () {

  webforms();
  //tracking();

});

/*/////////////////////////////////////////////////////////////////////////////////////////*/

function webforms() {

  if (document.querySelector('.webform-submission-form') !== null) {

    jQuery('.webform-submission-form').each(function() {
      var development_id = jQuery('.cameron-details').attr("data-developmentid");
      var property_id = jQuery('.cameron-details').attr("data-propertyid");
      var form_type = 'ask-question';
      var get_form_type = jQuery('.cameron-details').attr("data-formtype");
      if (get_form_type){ form_type = get_form_type }
      var get_brochure = jQuery('.cameron-details').attr("data-brochure");
      var get_sendto = jQuery('.cameron-details').attr("data-send");
      var coins_dev = jQuery('.cameron-details').attr("data-dev-coins");
      var coins_plot = jQuery('.cameron-details').attr("data-plot-coins");

      jQuery(".webform-submission-register-for-updates-form input[name='brochure']").val(get_brochure);
      jQuery(".webform-submission-form input[name='development']").val(development_id);
      jQuery(".webform-submission-form input[name='property']").val(property_id);
      jQuery(".webform-submission-form input[name='development_emails']").val(get_sendto);
      jQuery(".webform-submission-contact-form input[name='type']").val(form_type);
      jQuery(".webform-submission-contact-form input[name='coins']").val(coins_dev);
      jQuery(".webform-submission-contact-form input[name='coins_plot']").val(coins_plot);
    });

  }

}


function tracking(){



  var receiveMessage = function(event){
    var data = event.data;
    var eventName = data.substring(0,data.indexOf('.'));
    var booking = data.substring(data.indexOf('.')+1);

    if(eventName === 'appointeddBookingConfirmed'){
      try{
        ga('send', {
          hitType: 'event',
          eventCategory: 'booking',
          eventAction: 'newbooking',
          eventLabel: ''
        });
        dataLayer.push({'event': 'Booking Success'});
      }
      catch(err){
        console.warn('Failed getting JSON from event ' + eventName, err);
        console.warn(booking);
      }
    }
  };
  window.addEventListener("message", receiveMessage, false);
}

/*/////////////////////////////////////////////////////////////////////////////////////////*/
