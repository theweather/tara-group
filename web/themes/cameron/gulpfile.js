
const { src, dest, watch, series, parallel } = require('gulp');

const sass = require('gulp-sass');
const stripCssComments = require('gulp-strip-css-comments');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

const vendor = 'node_modules';

const basePaths = {
    styleSource: 'source/scss/*.scss',
    scriptSource: 'source/js/*.js',
    styleBuild: 'build/css',
    scriptBuild: 'build/js'
}

const scriptAssets = [
    vendor + '/@glidejs/glide/dist/glide.js',
    vendor + '/photoswipe/dist/photoswipe.js',
    vendor + '/photoswipe/dist/photoswipe-ui-default.js',
    basePaths.scriptSource
]

const sassOptions = {
    errLogToConsole: true,
    outputStyle: 'compressed'
}

function scssTask(){
    return src(basePaths.styleSource)
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(stripCssComments({preserve: false}))
        .pipe(rename({
            extname: '-min.css'
        }))
        .pipe(dest(basePaths.styleBuild)
        );
}

function jsTask(){
    return src(scriptAssets)
        .pipe(concat('global.js'))
        .pipe(uglify())
        .pipe(rename({
            extname: '-min.js'
        }))
        .pipe(dest(basePaths.scriptBuild)
        );
}

function watchTask(){
    watch([basePaths.styleSource, basePaths.scriptSource],
        series(
            parallel(scssTask, jsTask)
        )
    );
}

exports.default = series(
    parallel(scssTask, jsTask),
    watchTask
);
